package com.besafe.ui.signup;


import com.besafe.model.User;
import com.besafe.ui.base.MvpPresenter;
import com.besafe.ui.base.MvpView;

public interface SignUpContract {

    interface View extends MvpView {
        void onSignUpSuccess(User user);
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
        void signUp(String email, String password, User user);
    }
}
