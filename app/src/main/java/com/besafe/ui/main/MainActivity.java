package com.besafe.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.besafe.R;
import com.besafe.ui.base.BaseActivity;
import com.besafe.ui.home.HomeFragment;
import com.besafe.ui.profile.ProfileFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainContract.View {

    @BindView(R.id.app_bar)
    RelativeLayout appBar;
    @BindView(R.id.container)
    FrameLayout container;

    @Override
    protected void setUp() {
        replaceFragmentToActivity(new HomeFragment(), false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUp();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @OnClick({R.id.app_bar, R.id.container})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.app_bar:
                break;
            case R.id.container:
                break;
        }
    }
}
