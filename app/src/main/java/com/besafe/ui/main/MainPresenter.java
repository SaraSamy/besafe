package com.besafe.ui.main;

import com.besafe.data.DataManager;
import com.besafe.ui.base.BasePresenter;

public class MainPresenter extends BasePresenter implements MainContract.Presenter {
    public MainPresenter(DataManager dataManager) {
        super(dataManager);
    }
}
