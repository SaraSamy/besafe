package com.besafe.ui.main;

import com.besafe.ui.base.MvpPresenter;
import com.besafe.ui.base.MvpView;

public interface MainContract {

    public interface View extends MvpView {
    }

    public interface Presenter extends MvpPresenter {
    }
}
