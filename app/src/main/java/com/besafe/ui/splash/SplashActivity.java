package com.besafe.ui.splash;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.google.firebase.auth.FirebaseAuth;
import com.besafe.R;
import com.besafe.ui.login.LoginActivity;
import com.besafe.ui.main.MainActivity;

public class SplashActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        (new Handler()).postDelayed((Runnable) (new Runnable() {
            public final void run() {
                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                Log.e("act.view", String.valueOf((mAuth.getCurrentUser()== null)));
                SplashActivity.this.startActivity(( mAuth.getCurrentUser()) == null ?
                        LoginActivity.getStartIntent( SplashActivity.this):
                        MainActivity.getStartIntent(SplashActivity.this));
                SplashActivity.this.finish();
            }
        }), 1000L);
    }
}
