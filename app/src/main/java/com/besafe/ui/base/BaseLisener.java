package com.besafe.ui.base;


public interface BaseLisener<T, E,K> {

    void onSuccess(T data);

    void onSuccessData(T data, K key);

    void onFail(E error);
}
