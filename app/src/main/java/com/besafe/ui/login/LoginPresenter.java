package com.besafe.ui.login;


import com.besafe.R;
import com.besafe.data.DataManager;
import com.besafe.model.User;
import com.besafe.ui.base.BaseLisener;
import com.besafe.ui.base.BasePresenter;

public class LoginPresenter <V extends LoginContract.View> extends BasePresenter<V>
        implements LoginContract.Presenter<V> , BaseLisener<User, String,String> {

    public LoginPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void login(String email, String password) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            getDataManager().login(email, password, this);
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }

    @Override
    public void onSuccess(User user) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
          getMvpView().onLoginSuccess(user);
        }
    }

    @Override
    public void onSuccessData(User data, String key) {

    }

    @Override
    public void onFail(String error) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            getMvpView().showMessage(error);
        }
    }
}
