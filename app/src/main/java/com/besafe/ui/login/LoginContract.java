package com.besafe.ui.login;


import com.besafe.model.User;
import com.besafe.ui.base.MvpPresenter;
import com.besafe.ui.base.MvpView;

public interface LoginContract {

    public interface View extends MvpView {
        void onLoginSuccess(User user);
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
    void login(String email, String password);
    }
}