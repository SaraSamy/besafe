package com.besafe.ui.profile;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.besafe.R;
import com.besafe.data.AppDataManager;
import com.besafe.model.User;
import com.besafe.ui.base.BaseFragment;
import com.besafe.ui.home.MapActivity;
import com.besafe.ui.login.LoginActivity;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.marcoscg.dialogsheet.DialogSheet;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.TedRxBottomPicker;
import kotlin.jvm.internal.Intrinsics;

import static android.content.Context.LOCATION_SERVICE;
import static androidx.core.content.ContextCompat.checkSelfPermission;
import static com.firebase.ui.auth.AuthUI.getApplicationContext;


public class ProfileFragment extends BaseFragment implements ProfileContract.View {

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.changeStatus)
    Button changeStatus;
    @BindView(R.id.logoutB)
    Button logoutB;
    private ProfilePresenter presenter;
    private Boolean flag = false;
    User user;
    @BindView(R.id.setting)
    ImageView setting;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void setUp(View view) {
        presenter = new ProfilePresenter(new AppDataManager());
        presenter.onAttach(this);
        presenter.getUser();
        setting.setOnClickListener(view1 -> {
            SettingBS();
        });

    }

    public static ProfileFragment newInstance(@NotNull String title) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        return new ProfileFragment();
    }

    @Override
    public void onSuccess(User userBack) {
        View view = getView();
        if (view != null) {
            if (userBack.getImg() != null) {
                Glide.with(this).load(userBack.getImg()).into(image);
            }
            user=userBack;
            name.setText(userBack.getName());

            if (userBack.getPhone() != null) {
                phone.setText(userBack.getPhone());
            }
            if (userBack.getStatus() != null) {
                changeStatus.setText(userBack.getStatus());

                if (userBack.getStatus().equals("Status negative")) {
                    changeStatus.getBackground().setColorFilter(ContextCompat.getColor(getContext(), android.R.color.holo_green_dark), PorterDuff.Mode.MULTIPLY);
                } else {
                    changeStatus.getBackground().setColorFilter(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark), PorterDuff.Mode.MULTIPLY);
                }
            }


            view.findViewById(R.id.logoutB).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FirebaseAuth.getInstance().signOut();
                    Intent logout = new Intent(getContext(), LoginActivity.class);
                    logout.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(logout);
                    getActivity().finish();

                }
            });
        }
    }

    void changeStatusBS() {
        DialogSheet dialogSheet = new DialogSheet(Objects.requireNonNull(getContext()));
        dialogSheet.setTitle("Change status")
                .setColoredNavigationBar(true)
                .setView(R.layout.change_status_bottom_sheet)
                .setTitleTextSize(20)
                .setButtonsColorRes(R.color.colorAccent)
                .show();
        View inflatedView = dialogSheet.getInflatedView();
        Button changeB = inflatedView.findViewById(R.id.changeB);

        changeB.setOnClickListener(view -> {
            getBaseActivity().showLoading();
            getLocation();
            user.setStatus(changeB.getText().toString().equals("Status positive") ? "Status negative" : "Status positive");
            FirebaseDatabase.getInstance("https://besafe-4ab90-default-rtdb.firebaseio.com/").getReference()
                    .child("users").child(FirebaseAuth.getInstance().getUid())
                    .setValue(user)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    getBaseActivity().hideLoading();
                    if (changeB.getText().toString().equals("Status negative")) {
                        changeStatus.setText("Status positive");
                        changeStatus.getBackground().setColorFilter(ContextCompat.getColor(getContext(), android.R.color.holo_red_dark), PorterDuff.Mode.MULTIPLY);
                    } else {
                        changeStatus.setText("Status negative");
                        changeStatus.getBackground().setColorFilter(ContextCompat.getColor(getContext(), android.R.color.holo_green_dark), PorterDuff.Mode.MULTIPLY);
                    }

                    Toast.makeText(getContext(), "Changed", Toast.LENGTH_SHORT).show();
                }
            });
            dialogSheet.dismiss();

        });
    }


    void SettingBS() {
        DialogSheet dialogSheet = new DialogSheet(Objects.requireNonNull(getContext()));
        dialogSheet.setTitle("Update profile")
                .setColoredNavigationBar(true)
                .setView(R.layout.bottom_sheet_edit_profile)
                .setTitleTextSize(20)
                .setButtonsColorRes(R.color.colorAccent)
                .show();
        View inflatedView = dialogSheet.getInflatedView();
        Button add = inflatedView.findViewById(R.id.addB);
        ImageView usernewIV = inflatedView.findViewById(R.id.userIV);
        ImageView imageChooser = inflatedView.findViewById(R.id.imageChooser);
        EditText nameET = inflatedView.findViewById(R.id.nameET);
        nameET.setText(user.getName());
        if (user.getImg() != null) {
            Glide.with(this).load(user.getImg()).into(usernewIV);
        }
        imageChooser.setOnClickListener(view -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED &&

                        checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED &&

                        checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {

                    requestPermissions(new String[]{Manifest.permission.CAMERA,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            100);
                    return;
                }
            }
            TedRxBottomPicker.with(getActivity())
                    .show()
                    .subscribe(uri -> {
                                user.setImg(uri.toString());
                                flag = true;
                                Glide.with(this)
                                        .load(uri)
                                        .into(usernewIV);
                            },
                            throwable -> {
                            });
        });
        add.setOnClickListener(view -> {
            if (nameET.getText().toString().isEmpty()) {
                nameET.setError("Name is required");
                nameET.requestFocus();
                return;
            }else {
                user.setName(nameET.getText().toString());
                presenter.update(user, flag);
                dialogSheet.dismiss();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        changeStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeStatusBS();
            }
        });
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            Location l = null;
            LocationManager mLocationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
            List<String> providers = mLocationManager.getProviders(true);
            Location bestLocation = null;

            for (String provider : providers) {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    l = mLocationManager.getLastKnownLocation(provider);
                }
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = l;
                }
            }
            assert l != null;
            user.setLatitude(l.getLatitude());
            user.setLongitude(l.getLongitude());

//            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//            if (locationGPS != null) {
//                double lat = locationGPS.getLatitude();
//                double longi = locationGPS.getLongitude();
//                latitude = lat;
//                longitude = longi;
//            } else {
//                Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
//            }
        }
    }
}
