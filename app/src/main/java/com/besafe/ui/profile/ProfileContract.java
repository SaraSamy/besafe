package com.besafe.ui.profile;

import com.besafe.model.User;
import com.besafe.ui.base.MvpPresenter;
import com.besafe.ui.base.MvpView;

public interface ProfileContract {
     interface View extends MvpView {
        void onSuccess(User user);
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
        void getUser();
        void update(User user, Boolean flag);
    }
}
