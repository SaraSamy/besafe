package com.besafe.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.besafe.R;
import com.besafe.ui.base.BaseFragment;

import org.jetbrains.annotations.NotNull;

import butterknife.ButterKnife;
import kotlin.jvm.internal.Intrinsics;

public class SaveFragment extends BaseFragment {

    public SaveFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_save_covid, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void setUp(View view) {

    }
    public static SaveFragment newInstance(@NotNull String title) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        return new SaveFragment();
    }
}
