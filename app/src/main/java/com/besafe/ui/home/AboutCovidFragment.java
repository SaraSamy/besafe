package com.besafe.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.besafe.R;
import com.besafe.ui.base.BaseFragment;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import butterknife.ButterKnife;
import kotlin.jvm.internal.Intrinsics;

public class AboutCovidFragment extends BaseFragment {

    public AboutCovidFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_covid, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void setUp(View view) throws IOException {
        MyGETRequest();
    }
    public static AboutCovidFragment newInstance(@NotNull String title) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        return new AboutCovidFragment();
    }


    public static void MyGETRequest() throws IOException {

    }
}
