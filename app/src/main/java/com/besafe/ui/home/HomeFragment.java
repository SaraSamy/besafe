package com.besafe.ui.home;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.besafe.R;
import com.besafe.ui.base.BaseFragment;
import com.besafe.ui.profile.ProfileFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment {
    @BindView(R.id.profileCV)
    CardView profileCV;
    @BindView(R.id.aboutCovidCV)
    CardView aboutCovidCV;
    @BindView(R.id.followCV)
    CardView followCV;
    @BindView(R.id.avoidCV)
    CardView avoidCV;
    @BindView(R.id.mapCV)
    CardView mapCV;
//    @BindView(R.id.closedTendersCV)
//    CardView closedTendersCV;
//    @BindView(R.id.allCompaniesCV)
//    CardView allCompaniesCV;
//    @BindView(R.id.adminV)
//    LinearLayout adminV;

    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void setUp(View view) {

        profileCV.setOnClickListener(view1 -> {
           getBaseActivity().replaceFragmentToActivity(ProfileFragment.newInstance("Profile"), true);
        });
        aboutCovidCV.setOnClickListener(view1 -> {
            getBaseActivity().replaceFragmentToActivity(AboutCovidFragment.newInstance("About Covid19"), true);
        });
        followCV.setOnClickListener(view1 -> {
            getBaseActivity().replaceFragmentToActivity(SaveFragment.newInstance("Protect  yourself"), true);
        });
        avoidCV.setOnClickListener(view1 -> {
            getBaseActivity().replaceFragmentToActivity(AvoidFragment.newInstance(""), true);
        });
        mapCV.setOnClickListener(view1 -> {
            OnGPS();

        });

    }
    private void OnGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Want to know your current location,Enable GPS").setCancelable(false).setPositiveButton("Yes", new  DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              //  startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                startActivity(MapActivity.getStartIntent(getContext()));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
