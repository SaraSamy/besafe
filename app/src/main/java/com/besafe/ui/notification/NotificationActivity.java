package com.besafe.ui.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.besafe.R;
import com.besafe.data.AppDataManager;
import com.besafe.model.Notification;
import com.besafe.ui.base.BaseActivity;
import com.besafe.ui.notification.adapter.NotificationAdapter;
import com.besafe.utils.ViewUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.GridSpacingItemDecoration;

public class NotificationActivity extends BaseActivity implements NotificationAdapter.NotificationI, NotificationContract.View {
    @BindView(R.id.notificationRV)
    RecyclerView notificationRV;
    @BindView(R.id.NoDataIV)
    ImageView NoDataIV;
    @BindView(R.id.NoDataTV)
    TextView NoDataTV;
    NotificationAdapter adapter;
    private ArrayList<Notification> notifications = new ArrayList<>();
    NotificationPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        setUp();
    }

    @Override
    protected void setUp() {
        notifications = new ArrayList<>();
        adapter = new NotificationAdapter(notifications, this);
        presenter = new NotificationPresenter(new AppDataManager());
        presenter.onAttach(this);

        presenter.getNotification(FirebaseAuth.getInstance().getUid());
        setUpRecyclers(notificationRV);
    }

    private void setUpRecyclers(@NotNull RecyclerView recyclerView) {
        GridLayoutManager girdLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration((new GridSpacingItemDecoration(1, ViewUtils.dpToPx(8.0F), true)));
    }


    @Override
    public void notificationView(Notification notification) {
//        getBaseActivity().replaceFragmentToActivity(OneTenderFragment.newInstance("",tender), true);
    }

    @Override
    public void notificationCome(Notification notification) {
        findViewById(R.id.NoDataTV).setVisibility(View.GONE);
        findViewById(R.id.NoDataIV).setVisibility(View.GONE);
        findViewById(R.id.notificationRV).setVisibility(View.VISIBLE);

        adapter.addNotificationVH(notification);
    }

    @Override
    public void empty() {
        findViewById(R.id.NoDataTV).setVisibility(View.VISIBLE);
        findViewById(R.id.NoDataIV).setVisibility(View.VISIBLE);
        findViewById(R.id.notificationRV).setVisibility(View.GONE);

    }

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, NotificationActivity.class);
        return intent;
    }

}
