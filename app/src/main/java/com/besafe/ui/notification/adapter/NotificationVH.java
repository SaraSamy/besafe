package com.besafe.ui.notification.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.besafe.R;
import com.besafe.model.Notification;

import butterknife.BindView;
import butterknife.ButterKnife;


class NotificationVH extends RecyclerView.ViewHolder {

    @BindView(R.id.titleTV)
    TextView titleTV;
    @BindView(R.id.bodyTV)
    TextView bodyTV;
    @BindView(R.id.notificationIV)
    ImageView notificationIV;

    NotificationVH(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind( Notification notification) {
        titleTV.setText(notification.getTitle());
        bodyTV.setText(notification.getBody());
        Glide.with(itemView.getContext()).load(notification.getImg()).into(notificationIV);
    }
}
