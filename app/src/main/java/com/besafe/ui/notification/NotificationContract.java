package com.besafe.ui.notification;


import com.besafe.model.Notification;
import com.besafe.ui.base.MvpPresenter;
import com.besafe.ui.base.MvpView;

public interface NotificationContract {
     interface View extends MvpView {
        void notificationCome(Notification notification);
         void empty();
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
         void getNotification(String eq);
    }
}
