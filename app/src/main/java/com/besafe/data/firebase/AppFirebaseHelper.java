package com.besafe.data.firebase;
import android.net.Uri;

import androidx.annotation.NonNull;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.besafe.model.User;
import com.besafe.ui.base.BaseLisener;

public class AppFirebaseHelper implements FirebaseHelper {

    private FirebaseDatabase database;
    private FirebaseAuth auth;
    private DatabaseReference myRef;

    public AppFirebaseHelper() {
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance("https://besafe-4ab90-default-rtdb.firebaseio.com/");
        myRef = database.getReference();
    }

    @Override
    public Task<Void> updateUser(User user, BaseLisener<User, String, String> lisener) {
        return myRef.child("users").child(user.getId()).setValue(user);
    }


    @Override
    public Task<AuthResult> signUp(String email, String password) {
        return auth.createUserWithEmailAndPassword(email.trim(), password);
    }

    @Override
    public Task<Void> postUser(String uId, User user) {
        return myRef.child("users").child(uId).setValue(user);
    }

    @Override
    public Task<AuthResult> login(String email, String password) {
        return auth.signInWithEmailAndPassword(email.trim(), password);
    }


    @Override
    public void getUser(String uId, final BaseLisener<User, String, String> lisener) {
        myRef.child("users").child(uId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user != null)
                        user.setId(dataSnapshot.getKey());
                    lisener.onSuccess(user);
                } else
                    lisener.onFail("User not found");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                lisener.onFail(databaseError.getMessage());
            }
        });
    }

    @Override
    public Task<Void> postNotification(Object data) {
        return myRef.child("notification").push().setValue(data);
    }

    @Override
    public UploadTask uploadImage(Uri img) {
        return  FirebaseStorage.getInstance().getReference( System.currentTimeMillis() + ".jpg").putFile(img);
    }
}
