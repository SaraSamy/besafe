package com.besafe.data.firebase;


import android.net.Uri;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.storage.UploadTask;
import com.besafe.model.User;
import com.besafe.ui.base.BaseLisener;

public interface FirebaseHelper {

    Task<AuthResult> signUp(String email, String password);

    Task<AuthResult> login(String email, String password);

    Task<Void>  postUser(String uId, User user);

    Task<Void> updateUser(User user, BaseLisener<User, String, String> lisener);

    void  getUser(String uId, BaseLisener<User, String, String> lisener);

    UploadTask uploadImage(Uri img);

    Task <Void> postNotification(Object data);
}
