package com.besafe.data;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.UploadTask;
import com.besafe.data.firebase.AppFirebaseHelper;
import com.besafe.data.firebase.FirebaseHelper;
import com.besafe.model.PostTopic;
import com.besafe.model.User;
import com.besafe.ui.base.BaseLisener;


public class AppDataManager implements DataManager {

    private FirebaseHelper firebaseAppHelper;

    public AppDataManager(){
        firebaseAppHelper=new AppFirebaseHelper();
    }

    @Override
    public void signUp(String email, String password, final BaseLisener<User, String, String> lisener, final User user) {
        firebaseAppHelper.signUp(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull final Task<AuthResult> taskAuth) {
                if (taskAuth.isSuccessful()) {
                    firebaseAppHelper.postUser(taskAuth.getResult().getUser().getUid(),user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                FirebaseMessaging.getInstance().subscribeToTopic("rentItem")
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                            }
                                        });
                                FirebaseMessaging.getInstance().subscribeToTopic("tutorials")
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                firebaseAppHelper.getUser(taskAuth.getResult().getUser().getUid(), lisener);
                                            }
                                        });

                            }
                            else {
                                lisener.onFail(task.getException().getMessage());
                            }
                        }
                    });
                }
                else
                    lisener.onFail(taskAuth.getException().getMessage());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                lisener.onFail(e.getMessage());
            }
        });
    }

    @Override
    public void login(String email, String password, final BaseLisener<User, String, String> lisener) {
        firebaseAppHelper.login(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful())
                {
                    FirebaseMessaging.getInstance().subscribeToTopic("tutorials")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                }
                            });
                    FirebaseMessaging.getInstance().subscribeToTopic("rentItem")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                }
                            });
                    firebaseAppHelper.getUser(task.getResult().getUser().getUid(), lisener);
                }

                else
                    lisener.onFail(task.getException().getMessage());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                lisener.onFail(e.getMessage());
            }
        });
    }

    @Override
    public void getUser(String uId, BaseLisener<User, String, String> lisener){
        firebaseAppHelper.getUser(uId, lisener);
    }

    @Override
    public UploadTask uploadImage(Uri img) {
        return firebaseAppHelper.uploadImage(img);
    }

    @Override
    public void notification(Object data , PostTopic topic, BaseLisener lisener) {

    }

    @Override
    public Task<Void> updateUser(User user, BaseLisener<User, String, String> lisener) {
        return firebaseAppHelper.updateUser(user, lisener).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    lisener.onSuccess(user);
                } else
                    lisener.onFail(task.getException().getMessage());
            }
        });
    }


}
