package com.besafe.data;

import android.net.Uri;

import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.UploadTask;
import com.besafe.model.PostTopic;
import com.besafe.model.User;
import com.besafe.ui.base.BaseLisener;

public interface DataManager {

    void signUp(String email, String password, BaseLisener<User, String, String> lisener, User user);

    void login(String email, String password, BaseLisener<User, String, String> lisener);

    void getUser(String uId, BaseLisener<User, String, String> lisener);

    UploadTask uploadImage(Uri img);

    void  notification(Object data, PostTopic topic, BaseLisener lisener);
    Task<Void> updateUser(User user, BaseLisener<User, String,String> lisener);

}
