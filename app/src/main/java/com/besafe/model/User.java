package com.besafe.model;

import java.io.Serializable;


public class User implements Serializable, Comparable {
    private String name;
    private String img = "https://firebasestorage.googleapis.com/v0/b/photographer-62d5a.appspot.com/o/account.png?alt=media&token=df2975b7-72b3-4a3e-ab9c-43b0b3849011";
    private String id;
    private String type;
    private String phone;
    private double latitude;
    private double longitude;
    double distance;
    String status="Status negative";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User(String name, String img, String id, String type, String phone, double latitude, double longitude, double distance, String status) {
        this.name = name;
        this.img = img;
        this.id = id;
        this.type = type;
        this.phone = phone;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
        this.status = status;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public User() {
    }


    @Override
    public int compareTo(Object photographer) {
        double comparePoints=((User)photographer).getDistance();
        return (int)(comparePoints-this.getDistance());
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", img='" + img + '\'' +
                ", id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", phone='" + phone + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public User(String name, String img, String id, String type, String phone,  double latitude, double longitude) {
        this.name = name;
        this.img = img;
        this.id = id;
        this.type = type;
        this.phone = phone;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
