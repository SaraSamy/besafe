package com.besafe.model;

public class Notification {
    private String title;
    private String body;
    private String type;
    private String id;
    private String img;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public Notification() {
    }

    public String getImg() {
        return img;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", type='" + type + '\'' +
                ", id='" + id + '\'' +
                ", img='" + img + '\'' +
                '}';
    }

    public Notification(String title, String body, String type, String id, String img) {
        this.title = title;
        this.body = body;
        this.type = type;
        this.id = id;
        this.img = img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
